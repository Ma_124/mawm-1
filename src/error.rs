use err_derive::Error;
use std::{borrow::Cow, sync::mpsc::SendError};
use x11rb::rust_connection::{ConnectError, ConnectionError, ReplyError};

use crate::{wm::Event, x::PropertyError};

/// An error causing the whole window manager to crash.
#[derive(Debug, Error)]
pub enum FatalError {
    #[error(display = "failed to connect: {}", _0)]
    XConnectError(#[source] ConnectError),

    #[error(display = "connection failed: {}", _0)]
    XConnectionError(#[source] ConnectionError),

    #[error(display = "x error: {}", _0)]
    XReplyError {
        #[source]
        error: ReplyError,
        function: Cow<'static, str>,
    },

    #[error(display = "worker crashed: {}", _0)]
    SendError(#[source] SendError<Event>),

    #[error(display = "worker crashed")]
    WorkerCrashed,
}

/// An isolated error which will *not* crash the whole window manager.
#[derive(Debug, Error)]
pub enum Error {
    #[error(display = "connection failed: {}", _0)]
    XConnectionError(#[source] ConnectionError),

    #[error(display = "x error: {}", _0)]
    XReplyError {
        #[source]
        error: ReplyError,
        function: Cow<'static, str>,
    },

    #[error(display = "property error: {}", _0)]
    PropertyError(#[source] PropertyError),
}
