use std::{collections::BTreeMap, sync::mpsc::Receiver};

use tracing::{debug, trace, warn};
use x11rb::protocol::xproto::Window;

use crate::{client::Client, x::XState, Error, FatalError};

/// Events caused by either the display server or
/// commands received through the pipe.
#[derive(Debug)]
pub enum Event {
    ManageRequest(Window),
    UnmanageRequest(Window),
    TitleChange(Window),
    ClassChange(Window),
    PidChange(Window),
    TypeChange(Window),
    StateChange(Window),
    GeometryChange(Window),
    SizeHintsChange(Window),
    TransientForChange(Window),
    HintsChange(Window),
    Crash,
    // TODO SpawnCommand (specify tag etc)
    // TODO Focus{Up,Down,Left,Right}
    // TODO KillClient
    // TODO SetLayout
    // TODO Toggle{Floating,AlwaysOnTop,...}
    // TODO ShowOnlyTag
    // TODO ShowTag
    // TODO MoveToTag
    // TODO CloneToTag
    // TODO GotoScreen
    // TODO Quit
    // TODO Restart
}

/// The state of the WM.
#[derive(Debug)]
pub struct Wm {
    pub x: XState,
    pub event_queue: Receiver<Event>,
    pub clients: BTreeMap<Window, Client>,
}

impl Wm {
    /// Adopt ([`Self::manage`]) all children.
    pub fn adopt_children(&mut self) -> Result<(), FatalError> {
        let children = self.x.children()?;

        // manage all existing clients and move
        // them to the right tags
        for child in children {
            self.manage(child, false);
        }

        // in the first pass all non-transient windows
        // were moved onto the right tags. now we can
        // move transient tags to their parents
        for client in self.clients.values() {
            client.move_to_transient_for();
        }

        Ok(())
    }

    pub fn manage(&mut self, w: Window, requests_map: bool) {
        if self.clients.contains_key(&w) {
            return;
        }

        let mut wrapper = || {
            let mut c = match self.x.new_client(w, requests_map, true)? {
                Some(c) => c,
                None => return Ok(()),
            };
            // TODO move into rules()
            c.props |= c.typ.default_properties();

            // TODO apply rules

            // TODO move onto preferred tags (`_NET_WM_DESKTOP`)

            debug!(client = ?c, "manage");
            self.clients.insert(w, c);
            self.x.subscribe_to_client_events(w)?;

            // TODO push to _NET_WM_CLIENTS

            Ok(())
        };

        if let Err(error) = wrapper() {
            let _: &Error = &error;

            warn!(?error, wid = w, "cannot manage window");
        }
    }

    pub fn unmanage(&mut self, _w: Window) {
        todo!("unmanage")
    }

    pub fn worker(&mut self) -> Result<(), FatalError> {
        loop {
            let ev = match self.event_queue.recv() {
                Ok(ev) => ev,
                Err(_) => return Ok(()),
            };

            trace!(event = ?ev, "worker event");

            match ev {
                Event::ManageRequest(w) => self.manage(w, true),
                Event::UnmanageRequest(w) => self.unmanage(w),
                Event::TitleChange(w)
                | Event::ClassChange(w)
                | Event::PidChange(w)
                | Event::TypeChange(w)
                | Event::StateChange(w)
                | Event::SizeHintsChange(w)
                | Event::TransientForChange(w)
                | Event::HintsChange(w) => {
                    let wrapper = || {
                        if let Some(c) = self.clients.get_mut(&w) {
                            self.x.update_client_property(c, ev)?;
                            Ok(())
                        } else {
                            Ok(())
                        }
                    };

                    if let Err(error) = wrapper() {
                        let _: Error = error;

                        warn!(?error, wid = w, "cannot update client property");
                    } else {
                        todo!("mark proper tables as dirty")
                    }
                }
                Event::GeometryChange(_) => todo!("geometry change"),
                Event::Crash => {
                    // TODO save state
                    return Ok(());
                }
            }
        }
    }
}
