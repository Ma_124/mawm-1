use std::{
    cmp::{min, Ordering},
    fmt::{self, Debug},
    mem::swap,
};

use bitmask::bitmask;
use x11rb::protocol::xproto::Window;

/// A client managed by mawm.
#[derive(Debug)]
pub struct Client {
    /// Window ID
    pub window: Window,

    /// Window Title
    pub title: String,

    /// Class name (second item of `WM_CLASS`)
    pub class: String,

    /// Instance name (first item of `WM_CLASS`)
    pub instance: String,

    /// Process id of the owning process.
    pub pid: u32,

    /// Type of the client.
    pub typ: ClientType,

    /// Geometry
    pub geometry: Geometry,

    /// Geometry when the window *was* floating
    pub old_geometry: Option<Geometry>,

    /// Size hints.
    pub size_hints: SizeHints,

    /// Window for which this is a popup
    pub transient_for: Option<Window>,

    /// Boolean properties
    pub props: ClientProperties,
}

/// Type of client.
#[derive(Debug, Clone, Copy)]
pub enum ClientType {
    Desktop,
    Dock,
    Toolbar,
    Menu,
    Utility,
    Splash,
    Dialog,
    DropdownMenu,
    PopupMenu,
    Tooltip,
    Notification,
    Combo,
    DragNDrop,
    Normal,
}

/// X and Y.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Position {
    pub x: i32,
    pub y: i32,
}

/// Width and height.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Dimensions {
    pub width: u32,
    pub height: u32,
}

/// A fraction representing an aspect ratio.
#[derive(Debug, Clone, Copy)]
pub struct AspectRatio {
    pub numerator: i32,
    pub denominator: i32,
}

impl AspectRatio {
    pub fn new(numerator: i32, denominator: i32) -> Self {
        Self {
            numerator,
            denominator,
        }
        .reduce()
    }

    fn reduce(self) -> Self {
        if self.denominator < 0 {
            Self {
                numerator: -self.numerator,
                denominator: -self.denominator,
            }
            .reduce()
        } else {
            let k = gcd(self.numerator.abs() as u32, self.denominator.abs() as u32);
            if k == 0 || k == 1 {
                self
            } else {
                Self {
                    numerator: self.numerator / (k as i32),
                    denominator: self.denominator / (k as i32),
                }
            }
        }
    }

    fn cmp(&self, other: Self) -> Option<Ordering> {
        if self.denominator == 0 || other.denominator == 0 {
            return None;
        }
        let a = self.reduce();
        let b = self.reduce();

        (a.numerator * b.denominator).partial_cmp(&(b.numerator * a.denominator))
    }
}

impl From<AspectRatio> for f64 {
    fn from(r: AspectRatio) -> Self {
        r.numerator as f64 / r.denominator as f64
    }
}

impl PartialEq for AspectRatio {
    fn eq(&self, other: &Self) -> bool {
        matches!(self.cmp(*other), Some(Ordering::Equal))
    }
}

impl PartialOrd for AspectRatio {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.cmp(*other)
    }
}

/// Window geometry.
#[derive(Debug, Clone, Copy)]
pub struct Geometry {
    pub position: Position,
    pub dimensions: Dimensions,
}

/// Size hint.
#[derive(Debug, Default, Clone, Copy)]
pub struct SizeHints {
    /// Minimum size
    pub min_size: Option<Dimensions>,

    /// Maximum size
    pub max_size: Option<Dimensions>,

    /// Minimum and maximum aspect ratio.
    ///
    /// If [`Self::base_size`] is given subtract it first.
    pub min_max_aspect: Option<(AspectRatio, AspectRatio)>,

    /// Base size.
    ///
    /// For use with [`Self::incr_size`].
    /// If [`None`] assume [`Self::min_size`].
    pub base_size: Option<Dimensions>,

    /// Increment size.
    ///
    /// Width and height increments for use with [`Self::base_size`].
    pub incr_size: Option<Dimensions>,
}

bitmask! {
    /// A collection of [`ClientProperty`]s.
    pub mask ClientProperties: u32 where
    #[derive(Debug)]
    /// A boolean property of a [`Client`].
    flags ClientProperty {
        /// Window is a modal dialog box (`_NET_WM_STATE_MODAL`)
        Modal            = 1<<0,

        /// Window is maximized both vertically and horizontal (`_NET_WM_STATE_MAXIMIZED_VERT | _NET_WM_STATE_MAXIMIZED_HORZ`)
        Maximized        = 1<<1,

        /// Window is minimized (`_NET_WM_STATE_HIDDEN`)
        Minimized        = 1<<2,

        /// Window is fullscreen (`_NET_WM_STATE_FULLSCREEN`)
        Fullscreen       = 1<<3,

        /// Window should always be above normal windows (`_NET_WM_STATE_ABOVE`)
        AboveNormal      = 1<<4,

        /// Window should always be below normal windows (`_NET_WM_STATE_BELOW`)
        BelowNormal      = 1<<5,

        /// Window demands attention (set by client or WM when a focus request was denied) (`_NET_WM_STATE_DEMANDS_ATTENTION`)
        DemandsAttention = 1<<6,

        /// Window is in focus (`_NET_WM_STATE_FOCUSED`)
        Focused          = 1<<7,

        /// Window has a fixed size
        Fixed            = 1<<8,

        /// Window is floating
        Floating         = 1<<9,

        /// Window is visible on all tags (`_NET_WM_STATE_STICKY`)
        Sticky           = 1<<10,

        /// Window is urgent
        Urgent           = 1<<11,

        /// Window should never be focused
        NeverFocus       = 1<<12,

        /// Window was floating before being maximized/set to fullscreen
        WasFloating      = 1<<13,

        // NOTE: all properties must be mirrored in Debug impl
    }
}

impl Client {
    pub fn move_to_transient_for(&self) {
        if let Some(_parent) = self.transient_for {
            // TODO move onto the same tags as parent
        }
    }
}

impl Default for ClientType {
    fn default() -> Self {
        Self::Normal
    }
}

impl ClientType {
    pub fn default_properties(self) -> ClientProperties {
        match self {
            Self::Desktop => ClientProperty::Fullscreen.into(),
            Self::Splash
            | Self::Dialog
            | Self::DropdownMenu
            | Self::PopupMenu
            | Self::Tooltip
            | Self::Notification
            | Self::Combo
            | Self::DragNDrop => ClientProperty::Floating.into(),
            _ => ClientProperties::none(),
        }
    }
}

impl AspectRatio {}

impl Debug for ClientProperties {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "ClientProperties(")?;
        write!(f, "0")?;
        for m in [
            ClientProperty::Modal,
            ClientProperty::Maximized,
            ClientProperty::Minimized,
            ClientProperty::Fullscreen,
            ClientProperty::AboveNormal,
            ClientProperty::BelowNormal,
            ClientProperty::DemandsAttention,
            ClientProperty::Focused,
            ClientProperty::Fixed,
            ClientProperty::Floating,
            ClientProperty::Sticky,
            ClientProperty::Urgent,
            ClientProperty::NeverFocus,
            ClientProperty::WasFloating,
        ] {
            if self.contains(m) {
                if f.alternate() {
                    write!(f, "\n  | ")?
                } else {
                    write!(f, " | ")?;
                }
                write!(f, "{:?}", m)?;
            }
        }
        write!(f, ")")
    }
}

/// Implementation of Stein's GCD algorithm.
fn gcd(mut n: u32, mut m: u32) -> u32 {
    if n == 0 {
        return m;
    } else if m == 0 {
        return n;
    }

    let k = {
        let k_n = n.trailing_zeros();
        let k_m = m.trailing_zeros();
        n >>= k_n;
        m >>= k_m;
        min(k_n, k_m)
    };

    loop {
        if n > m {
            swap(&mut n, &mut m);
        }
        m -= n;

        if m == 0 {
            return n << k;
        }

        m >>= m.trailing_zeros();
    }
}
