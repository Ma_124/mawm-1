#![feature(thread_is_running)]
#![doc = include_str!("../README.md")]

pub mod x;

mod client;
mod error;
mod wm;
pub use client::*;
pub use error::*;
pub use wm::*;

use std::{
    collections::BTreeMap,
    sync::{mpsc, Arc},
    thread::{self, JoinHandle},
};

use tracing::{error, trace, warn};
use x::XState;
use x11rb::{connection::Connection, protocol::Event as XEvent};

use crate::x::translate_event;

/// Run the window manager.
pub fn run() -> Result<(), FatalError> {
    let (conn, screen_num) = match x11rb::connect(None) {
        Ok(o) => o,
        Err(error) => {
            error!(hint = "is X running?");
            return Err(FatalError::XConnectError(error));
        }
    };

    let conn = Arc::new(conn);
    let (tx, rx) = mpsc::channel();

    let x = XState::new(conn.clone(), screen_num)?;
    let atoms = x.atoms();
    x.register_wm()?;

    let mut wm = Wm {
        x,
        event_queue: rx,
        clients: BTreeMap::new(),
    };
    wm.adopt_children()?;

    let worker = thread::spawn(move || wm.worker());

    loop {
        if worker.is_finished() {
            return if join_worker(worker) {
                Ok(())
            } else {
                Err(FatalError::WorkerCrashed)
            };
        }

        let ev = match conn.wait_for_event() {
            Ok(ev) => ev,
            Err(error) => {
                let _ = tx.send(Event::Crash);
                join_worker(worker);
                return Err(error.into());
            }
        };

        trace!(event = ?ev, "x event");

        match ev {
            XEvent::Error(e) => {
                warn!(error = ?e, "x error");
            }
            XEvent::Unknown(_) => {
                warn!("unknown event");
            }
            _ => {
                if let Some(ev) = translate_event(&atoms, &ev) {
                    tx.send(ev)?;
                } else {
                    warn!(event = ?ev, "unhandled event");
                }
            }
        }
    }
}

fn join_worker(worker: JoinHandle<Result<(), FatalError>>) -> bool {
    match worker.join() {
        Ok(Ok(())) => true,
        Ok(Err(error)) => {
            error!(?error, error_type = "error", "worker crashed");
            false
        }
        Err(error) => {
            error!(?error, error_type = "panic", "worker crashed");
            false
        }
    }
}
