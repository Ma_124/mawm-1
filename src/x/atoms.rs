use x11rb::protocol::xproto::Atom;
use x11rb::protocol::xproto::AtomEnum;

macro_rules! atoms {
    (
        wm:      { $($w_fn:ident ,)+ },
        net:     { $($n_fn:ident ,)+ },
        other:   { $($o_fn:ident ,)+ },
        builtin: { $($b_fn:ident ,)+ },
    ) => {
        ::x11rb::atom_manager! {
            pub Atoms: AtomsCookie {
                $($w_fn ,)+
                $($n_fn ,)+
                $($o_fn ,)+
            }
        }

        #[doc(hidden)]
        #[allow(unused, non_camel_case_types, clippy::upper_case_acronyms)]
        #[repr(usize)]
        enum __NetAtomCounter {
            $($n_fn ,)+
            num_of_elems
        }

        #[doc(hidden)]
        #[allow(unused, non_camel_case_types, clippy::upper_case_acronyms)]
        enum __NoDupedBuiltins {
            $($w_fn ,)+
            $($n_fn ,)+
            $($o_fn ,)+
            $($b_fn ,)+
        }

        impl Atoms {
            fn get_name(&self, a: Atom) -> Option<&'static str> {
                if false { unreachable!() }
                $(else if self.$w_fn == a { Some(stringify!($w_fn)) })+
                $(else if self.$n_fn == a { Some(stringify!($n_fn)) })+
                $(else if self.$o_fn == a { Some(stringify!($o_fn)) })+
                $(else if Atom::from(AtomEnum::$b_fn) == a { Some(stringify!($b_fn)) })+
                else {
                    None
                }
            }

            pub fn format(&self, a: Atom) -> String {
                if let Some(s) = &self.get_name(a) {
                    format!("Atom({})", s)
                } else {
                    format!("Atom({})", a)
                }
            }

            pub fn net_atoms(&self) -> [u32; __NetAtomCounter::num_of_elems as usize] {
                [
                    $(self.$n_fn ,)+
                ]
            }
        }
	};
}

atoms! {
    wm: {
        // https://tronche.com/gui/x/icccm/sec-4.html
        WM_PROTOCOLS,
        WM_DELETE_WINDOW,
        WM_STATE,
        WM_TAKE_FOCUS,
    },
    net: {
        // https://specifications.freedesktop.org/wm-spec/wm-spec-latest.html
        // https://en.wikipedia.org/wiki/Extended_Window_Manager_Hints
        _NET_SUPPORTED,
        _NET_CLIENT_LIST,
        _NET_NUMBER_OF_DESKTOPS,
        _NET_CURRENT_DESKTOP,
        _NET_DESKTOP_NAMES,
        _NET_ACTIVE_WINDOW,
        _NET_SUPPORTING_WM_CHECK,
        _NET_RESTACK_WINDOW,
        _NET_WM_NAME,
        _NET_WM_DESKTOP,
        _NET_WM_WINDOW_TYPE,
        _NET_WM_WINDOW_TYPE_DESKTOP,
        _NET_WM_WINDOW_TYPE_DOCK,
        _NET_WM_WINDOW_TYPE_TOOLBAR,
        _NET_WM_WINDOW_TYPE_MENU,
        _NET_WM_WINDOW_TYPE_UTILITY,
        _NET_WM_WINDOW_TYPE_SPLASH,
        _NET_WM_WINDOW_TYPE_DIALOG,
        _NET_WM_WINDOW_TYPE_DROPDOWN_MENU,
        _NET_WM_WINDOW_TYPE_POPUP_MENU,
        _NET_WM_WINDOW_TYPE_TOOLTIP,
        _NET_WM_WINDOW_TYPE_NOTIFICATION,
        _NET_WM_WINDOW_TYPE_COMBO,
        _NET_WM_WINDOW_TYPE_DND,
        _NET_WM_WINDOW_TYPE_NORMAL,
        _NET_WM_STATE,
        _NET_WM_STATE_MODAL,
        _NET_WM_STATE_STICKY,
        _NET_WM_STATE_MAXIMIZED_VERT,
        _NET_WM_STATE_MAXIMIZED_HORZ,
        _NET_WM_STATE_HIDDEN,
        _NET_WM_STATE_FULLSCREEN,
        _NET_WM_STATE_ABOVE,
        _NET_WM_STATE_BELOW,
        _NET_WM_STATE_DEMANDS_ATTENTION,
        _NET_WM_STATE_FOCUSED,
        _NET_WM_PID,
    },
    other: {
        UTF8_STRING,
    },
    builtin: {
        // have fixed values which can be found in [`AtomEnum`]
        NONE,
        ANY,
        PRIMARY,
        SECONDARY,
        ARC,
        ATOM,
        BITMAP,
        CARDINAL,
        COLORMAP,
        CURSOR,
        CUT_BUFFE_R0,
        CUT_BUFFE_R1,
        CUT_BUFFE_R2,
        CUT_BUFFE_R3,
        CUT_BUFFE_R4,
        CUT_BUFFE_R5,
        CUT_BUFFE_R6,
        CUT_BUFFE_R7,
        DRAWABLE,
        FONT,
        INTEGER,
        PIXMAP,
        POINT,
        RECTANGLE,
        RESOURCE_MANAGER,
        RGB_COLOR_MAP,
        RGB_BEST_MAP,
        RGB_BLUE_MAP,
        RGB_DEFAULT_MAP,
        RGB_GRAY_MAP,
        RGB_GREEN_MAP,
        RGB_RED_MAP,
        STRING,
        VISUALID,
        WINDOW,
        WM_COMMAND,
        WM_HINTS,
        WM_CLIENT_MACHINE,
        WM_ICON_NAME,
        WM_ICON_SIZE,
        WM_NAME,
        WM_NORMAL_HINTS,
        WM_SIZE_HINTS,
        WM_ZOOM_HINTS,
        MIN_SPACE,
        NORM_SPACE,
        MAX_SPACE,
        END_SPACE,
        SUPERSCRIPT_X,
        SUPERSCRIPT_Y,
        SUBSCRIPT_X,
        SUBSCRIPT_Y,
        UNDERLINE_POSITION,
        UNDERLINE_THICKNESS,
        STRIKEOUT_ASCENT,
        STRIKEOUT_DESCENT,
        ITALIC_ANGLE,
        X_HEIGHT,
        QUAD_WIDTH,
        WEIGHT,
        POINT_SIZE,
        RESOLUTION,
        COPYRIGHT,
        NOTICE,
        FONT_NAME,
        FAMILY_NAME,
        FULL_NAME,
        CAP_HEIGHT,
        WM_CLASS,
        WM_TRANSIENT_FOR,
    },
}
