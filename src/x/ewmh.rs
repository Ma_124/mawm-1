use x11rb::{
    properties::{WmHints, WmSizeHints},
    protocol::xproto::{Atom, AtomEnum, ConnectionExt as _, MapState, Window},
};

use crate::{
    client::{Client, ClientProperties, ClientProperty, Geometry},
    AspectRatio, ClientType, Dimensions, Error, Event, Position, SizeHints, Wm,
};

use super::{PropertyCookie, XState};

impl XState {
    /// Construct a [`Client`].
    pub fn new_client(
        &self,
        w: Window,
        requests_map: bool,
        honor_override_redirect: bool,
    ) -> Result<Option<Client>, Error> {
        let attrs = self
            .conn
            .get_window_attributes(w)?
            .reply()
            .map_err(|error| Error::XReplyError {
                error,
                function: "get window attributes".into(),
            })?;

        if attrs.override_redirect && honor_override_redirect {
            // this window does not want to be managed
            return Ok(None);
        }

        // Start async requests
        let title = self.req_title(w)?;
        let class = self.req_class(w)?;
        let pid = self.req_pid(w)?;
        let typ = self.req_type(w)?;
        let state = self.req_props(w)?;
        let hints = self.req_hints(w)?;
        let size_hints = self.req_size_hints(w)?;
        let transient_for = self.req_transient_for(w)?;

        let geometry = self.conn.get_geometry(w)?;

        // Collect replies
        let geometry = geometry.reply().map_err(|error| Error::XReplyError {
            error,
            function: "get geometry".into(),
        })?;

        let geometry = Geometry {
            position: Position {
                x: geometry.x as i32,
                y: geometry.y as i32,
            },
            dimensions: Dimensions {
                width: geometry.width as u32,
                height: geometry.height as u32,
            },
        };

        // TODO read strut

        let mut c = Client {
            window: w,
            title: String::new(),
            class: String::new(),
            instance: String::new(),
            pid: 0,
            typ: ClientType::Normal,
            geometry,
            old_geometry: None,
            size_hints: SizeHints::default(),
            transient_for: None,
            props: ClientProperties::none(),
        };

        self.update_title(&mut c, title)?;
        self.update_class(&mut c, class)?;
        self.update_pid(&mut c, pid)?;
        self.update_type(&mut c, typ)?;
        self.update_props(&mut c, state)?;
        self.update_hints(&mut c, hints)?;
        self.update_size_hints(&mut c, size_hints)?;
        self.update_transient_for(&mut c, transient_for)?;

        if !requests_map
            && !c.props.contains(ClientProperty::Minimized)
            && attrs.map_state != MapState::VIEWABLE
        {
            // window or one of it ancestors is unmapped eventhough it's not minimized

            // this check is only run when the window manager starts and other windows already exists.
            // In this rare case we waste time by waiting for all attributes before returning none.
            // In the general case of adopting windows as they're created it's faster to do all requests in parallel.
            return Ok(None);
        }

        Ok(Some(c))
    }

    fn req_title(&self, w: Window) -> Result<PropertyCookie<String>, Error> {
        Ok(self.get_string(w, self.atoms._NET_WM_NAME)?)
    }

    fn update_title(&self, c: &mut Client, p: PropertyCookie<String>) -> Result<(), Error> {
        c.title = p.reply()?.unwrap_or_default();
        Ok(())
    }

    fn req_class(&self, w: Window) -> Result<PropertyCookie<(String, String)>, Error> {
        Ok(self.get_class(w)?)
    }

    fn update_class(
        &self,
        c: &mut Client,
        p: PropertyCookie<(String, String)>,
    ) -> Result<(), Error> {
        (c.instance, c.class) = p.reply()?.unwrap_or_else(|| (String::new(), String::new()));
        Ok(())
    }

    fn req_pid(&self, w: Window) -> Result<PropertyCookie<u32>, Error> {
        Ok(self.get_card32(w, self.atoms._NET_WM_PID)?)
    }

    fn update_pid(&self, c: &mut Client, p: PropertyCookie<u32>) -> Result<(), Error> {
        c.pid = p.reply()?.unwrap_or_default();
        Ok(())
    }

    fn req_type(&self, w: Window) -> Result<PropertyCookie<Vec<u32>>, Error> {
        Ok(self.get_atoms(w, self.atoms._NET_WM_WINDOW_TYPE)?)
    }

    fn update_type(&self, c: &mut Client, p: PropertyCookie<Vec<u32>>) -> Result<(), Error> {
        let mut typ = None;
        for candidate in p.reply()?.unwrap_or_default() {
            let candidate = atom_to_type(self, candidate);
            if candidate.is_some() {
                typ = candidate;
                break;
            }
        }
        c.typ = typ.unwrap_or_default();
        Ok(())
    }

    fn req_props(&self, w: Window) -> Result<PropertyCookie<Vec<u32>>, Error> {
        Ok(self.get_atoms(w, self.atoms._NET_WM_STATE)?)
    }

    fn update_props(&self, c: &mut Client, p: PropertyCookie<Vec<u32>>) -> Result<(), Error> {
        c.props = ClientProperties::none();
        for prop in p.reply()?.unwrap_or_default() {
            c.props |= state_to_property(self, prop);
        }
        Ok(())
    }

    fn req_hints(&self, w: Window) -> Result<PropertyCookie<WmHints>, Error> {
        Ok(self.get_hints(w)?)
    }

    fn update_hints(&self, c: &mut Client, p: PropertyCookie<WmHints>) -> Result<(), Error> {
        let (never_focus, urgent) = p
            .reply()?
            .map(|hints| (!hints.input.unwrap_or(true), hints.urgent))
            .unwrap_or((false, false));
        if never_focus {
            c.props.set(ClientProperty::NeverFocus)
        } else {
            c.props.unset(ClientProperty::NeverFocus)
        }
        if urgent {
            c.props.set(ClientProperty::Urgent)
        } else {
            c.props.unset(ClientProperty::Urgent)
        }
        Ok(())
    }

    fn req_size_hints(&self, w: Window) -> Result<PropertyCookie<WmSizeHints>, Error> {
        Ok(self.get_size_hints(w, AtomEnum::WM_NORMAL_HINTS)?)
    }

    fn update_size_hints(
        &self,
        c: &mut Client,
        p: PropertyCookie<WmSizeHints>,
    ) -> Result<(), Error> {
        c.size_hints = p
            .reply()?
            .map(|h| SizeHints {
                min_size: opt_dimensions(h.min_size),
                max_size: opt_dimensions(h.max_size),
                min_max_aspect: h.aspect.map(|(min, max)| (min.into(), max.into())),
                base_size: opt_dimensions(h.base_size),
                incr_size: opt_dimensions(h.size_increment),
            })
            .unwrap_or_default();
        if c.size_hints.min_size == c.size_hints.max_size {
            c.props.set(ClientProperty::Fixed)
        } else {
            c.props.unset(ClientProperty::Fixed)
        }
        Ok(())
    }

    fn req_transient_for(&self, w: Window) -> Result<PropertyCookie<u32>, Error> {
        Ok(self.get_window_id(w, AtomEnum::WM_TRANSIENT_FOR)?)
    }

    fn update_transient_for(&self, c: &mut Client, p: PropertyCookie<u32>) -> Result<(), Error> {
        c.transient_for = p.reply()?;
        Ok(())
    }

    pub fn update_client_property(&mut self, c: &mut Client, ev: Event) -> Result<(), Error> {
        match ev {
            Event::TitleChange(w) => self.update_title(c, self.req_title(w)?),
            Event::ClassChange(w) => self.update_class(c, self.req_class(w)?),
            Event::PidChange(w) => self.update_pid(c, self.req_pid(w)?),
            Event::TypeChange(w) => self.update_type(c, self.req_type(w)?),
            Event::StateChange(w) => self.update_props(c, self.req_props(w)?),
            Event::SizeHintsChange(w) => self.update_size_hints(c, self.req_size_hints(w)?),
            Event::TransientForChange(w) => {
                self.update_transient_for(c, self.req_transient_for(w)?)
            }
            Event::HintsChange(w) => self.update_hints(c, self.req_hints(w)?),
            _ => todo!("wrong prop"),
        }
    }

    pub fn subscribe_to_client_events(&self, _w: Window) -> Result<(), Error> {
        // TODO subscribe_to_client_events
        Ok(())
    }
}

fn state_to_property(x: &XState, a: Atom) -> ClientProperties {
    if a == x.atoms._NET_WM_STATE_MODAL {
        ClientProperty::Modal.into()
    } else if a == x.atoms._NET_WM_STATE_MAXIMIZED_HORZ || a == x.atoms._NET_WM_STATE_MAXIMIZED_VERT
    {
        ClientProperty::Fullscreen.into()
    } else if a == x.atoms._NET_WM_STATE_HIDDEN {
        ClientProperty::Minimized.into()
    } else if a == x.atoms._NET_WM_STATE_FULLSCREEN {
        ClientProperty::Fullscreen.into()
    } else if a == x.atoms._NET_WM_STATE_ABOVE {
        ClientProperty::AboveNormal.into()
    } else if a == x.atoms._NET_WM_STATE_BELOW {
        ClientProperty::BelowNormal.into()
    } else if a == x.atoms._NET_WM_STATE_DEMANDS_ATTENTION {
        ClientProperty::DemandsAttention.into()
    } else if a == x.atoms._NET_WM_STATE_FOCUSED {
        ClientProperty::Focused.into()
    } else if a == x.atoms._NET_WM_STATE_STICKY {
        ClientProperty::Sticky.into()
    } else {
        ClientProperties::none()
    }
}

fn atom_to_type(x: &XState, a: Atom) -> Option<ClientType> {
    if a == x.atoms._NET_WM_DESKTOP {
        Some(ClientType::Desktop)
    } else if a == x.atoms._NET_WM_WINDOW_TYPE_DOCK {
        Some(ClientType::Dock)
    } else if a == x.atoms._NET_WM_WINDOW_TYPE_TOOLBAR {
        Some(ClientType::Toolbar)
    } else if a == x.atoms._NET_WM_WINDOW_TYPE_MENU {
        Some(ClientType::Menu)
    } else if a == x.atoms._NET_WM_WINDOW_TYPE_UTILITY {
        Some(ClientType::Utility)
    } else if a == x.atoms._NET_WM_WINDOW_TYPE_SPLASH {
        Some(ClientType::Splash)
    } else if a == x.atoms._NET_WM_WINDOW_TYPE_DIALOG {
        Some(ClientType::Dialog)
    } else if a == x.atoms._NET_WM_WINDOW_TYPE_DROPDOWN_MENU {
        Some(ClientType::DropdownMenu)
    } else if a == x.atoms._NET_WM_WINDOW_TYPE_POPUP_MENU {
        Some(ClientType::PopupMenu)
    } else if a == x.atoms._NET_WM_WINDOW_TYPE_TOOLTIP {
        Some(ClientType::Tooltip)
    } else if a == x.atoms._NET_WM_WINDOW_TYPE_NOTIFICATION {
        Some(ClientType::Notification)
    } else if a == x.atoms._NET_WM_WINDOW_TYPE_COMBO {
        Some(ClientType::Combo)
    } else if a == x.atoms._NET_WM_WINDOW_TYPE_DND {
        Some(ClientType::DragNDrop)
    } else if a == x.atoms._NET_WM_WINDOW_TYPE_NORMAL {
        Some(ClientType::Normal)
    } else {
        None
    }
}

fn opt_dimensions(dimensions: Option<(i32, i32)>) -> Option<Dimensions> {
    let (mut w, mut h) = dimensions?;

    if w < 0 {
        w = 0;
    }
    if h < 0 {
        h = 0;
    }

    Some(Dimensions {
        width: w as u32,
        height: h as u32,
    })
}

impl From<x11rb::properties::AspectRatio> for AspectRatio {
    fn from(r: x11rb::properties::AspectRatio) -> Self {
        Self {
            numerator: r.numerator,
            denominator: r.denominator,
        }
    }
}
