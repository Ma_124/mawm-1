use std::{
    fmt::{self, Display},
    sync::Arc,
};

use err_derive::Error;
use x11rb::{
    cookie::Cookie,
    properties::{WmClass, WmHints, WmSizeHints},
    protocol::xproto::{Atom, AtomEnum, ConnectionExt, GetPropertyReply, Window},
    rust_connection::{ConnectionError, ParseError, ReplyError, RustConnection},
};

use crate::{x::Atoms, x::XState};

const MAX_STRING_LEN: u32 = 1000;
const MAX_LIST_LEN: u32 = MAX_STRING_LEN / 4;

type CookieConvFn<'a, T> =
    Box<dyn Fn(GetPropertyReply, Arc<Atoms>) -> Result<T, PropertyErrorKind> + 'a>;

impl XState {
    fn get_prop<'a, T>(
        &'a self,
        w: Window,
        prop: impl Into<Atom>,
        typ: impl Into<Atom>,
        l: u32,
        conv: CookieConvFn<'a, T>,
    ) -> Result<PropertyCookie<'a, T>, ConnectionError> {
        let prop = prop.into();
        let cookie = self.conn.get_property(false, w, prop, typ, 0, l)?;
        Ok(PropertyCookie {
            window: w,
            atoms: self.atoms.clone(),
            prop,
            conv,
            cookie,
        })
    }

    fn get_u32(
        &self,
        w: Window,
        prop: impl Into<Atom>,
        typ_str: &'static str,
        typ: impl Into<Atom>,
    ) -> Result<PropertyCookie<'_, u32>, ConnectionError> {
        let typ = typ.into();
        self.get_prop(
            w,
            prop,
            typ,
            1,
            Box::new(move |reply, atoms| {
                if reply.type_ != typ {
                    Err(PropertyErrorKind::WrongType {
                        expected: typ_str,
                        got: atoms.format(reply.type_),
                    })
                } else {
                    reply
                        .value32()
                        .ok_or(PropertyErrorKind::WrongFormat {
                            expected: 32,
                            got: reply.format,
                        })?
                        .next()
                        .ok_or(PropertyErrorKind::WrongLength {
                            expected: 1,
                            got: 0,
                        })
                }
            }),
        )
    }

    /// Get the property `prop` of `w` with type `WINDOW`.
    pub fn get_window_id(
        &self,
        w: Window,
        prop: impl Into<Atom>,
    ) -> Result<PropertyCookie<'_, Window>, ConnectionError> {
        self.get_u32(w, prop, "WINDOW", AtomEnum::WINDOW)
    }

    /// Get the property `prop` of `w` with type `ATOM`.
    pub fn get_atom(
        &self,
        w: Window,
        prop: impl Into<Atom>,
    ) -> Result<PropertyCookie<'_, Atom>, ConnectionError> {
        self.get_u32(w, prop, "ATOM", AtomEnum::ATOM)
    }

    /// Get the property `prop` of `w` with type `CARDINAL` (`CARD32`).
    pub fn get_card32(
        &self,
        w: Window,
        prop: impl Into<Atom>,
    ) -> Result<PropertyCookie<'_, Atom>, ConnectionError> {
        self.get_u32(w, prop, "CARDINAL", AtomEnum::CARDINAL)
    }

    /// Get the property `prop` of `w` with type `STRING` or `UTF8_STRING`.
    pub fn get_string(
        &self,
        w: Window,
        prop: impl Into<Atom>,
    ) -> Result<PropertyCookie<'_, String>, ConnectionError> {
        self.get_prop(
            w,
            prop,
            AtomEnum::ANY,
            MAX_STRING_LEN,
            Box::new(move |reply, atoms| {
                if reply.type_ != u32::from(AtomEnum::STRING)
                    && reply.type_ != self.atoms.UTF8_STRING
                {
                    Err(PropertyErrorKind::WrongType {
                        expected: "STRING or UTF8_STRING",
                        got: atoms.format(reply.type_),
                    })
                } else {
                    let s = reply
                        .value8()
                        .ok_or(PropertyErrorKind::WrongFormat {
                            expected: 8,
                            got: reply.format,
                        })?
                        .collect::<Vec<_>>();
                    Ok(String::from_utf8_lossy(&s).into_owned())
                }
            }),
        )
    }

    /// Get the property `prop` of `w` with type list of `ATOM`.
    pub fn get_atoms(
        &self,
        w: Window,
        prop: impl Into<Atom>,
    ) -> Result<PropertyCookie<'_, Vec<Atom>>, ConnectionError> {
        self.get_prop(
            w,
            prop,
            AtomEnum::ATOM,
            MAX_LIST_LEN,
            Box::new(move |reply, atoms| {
                if reply.type_ != u32::from(AtomEnum::ATOM) {
                    Err(PropertyErrorKind::WrongType {
                        expected: "ATOM",
                        got: atoms.format(reply.type_),
                    })
                } else {
                    Ok(reply
                        .value32()
                        .ok_or(PropertyErrorKind::WrongFormat {
                            expected: 32,
                            got: reply.format,
                        })?
                        .collect::<Vec<Atom>>())
                }
            }),
        )
    }

    /// Get the `WM_CLASS` property (instance, class) property of `w`.
    pub fn get_class(
        &self,
        w: Window,
    ) -> Result<PropertyCookie<'_, (String, String)>, ConnectionError> {
        self.get_prop(
            w,
            AtomEnum::WM_CLASS,
            AtomEnum::STRING,
            MAX_STRING_LEN,
            Box::new(move |reply, atoms| {
                if reply.type_ != u32::from(AtomEnum::STRING) {
                    Err(PropertyErrorKind::WrongType {
                        expected: "STRING",
                        got: atoms.format(reply.type_),
                    })
                } else {
                    let class = WmClass::from_reply(reply)?;
                    Ok((
                        String::from_utf8_lossy(class.instance()).into_owned(),
                        String::from_utf8_lossy(class.class()).into_owned(),
                    ))
                }
            }),
        )
    }

    /// Get the `WM_HINTS` property property of `w`.
    pub fn get_hints(&self, w: Window) -> Result<PropertyCookie<'_, WmHints>, ConnectionError> {
        self.get_prop(
            w,
            AtomEnum::WM_HINTS,
            AtomEnum::WM_HINTS,
            // NUM_WM_HINTS_ELEMENTS from
            //   https://github.com/psychon/x11rb/blob/a6cc0d1/src/properties.rs#L450
            9,
            Box::new(move |reply, atoms| {
                if reply.type_ != u32::from(AtomEnum::WM_HINTS) {
                    Err(PropertyErrorKind::WrongType {
                        expected: "WM_HINTS",
                        got: atoms.format(reply.type_),
                    })
                } else {
                    Ok(WmHints::from_reply(&reply)?)
                }
            }),
        )
    }

    /// Get the property `prop` of `w` with type `WM_SIZE_HINTS`.
    pub fn get_size_hints(
        &self,
        w: Window,
        prop: impl Into<Atom>,
    ) -> Result<PropertyCookie<'_, WmSizeHints>, ConnectionError> {
        self.get_prop(
            w,
            prop,
            AtomEnum::WM_SIZE_HINTS,
            // NUM_WM_SIZE_HINTS_ELEMENTS from
            //   https://github.com/psychon/x11rb/blob/a6cc0d1/src/properties.rs#L164
            18,
            Box::new(move |reply, atoms| {
                if reply.type_ != u32::from(AtomEnum::WM_SIZE_HINTS) {
                    Err(PropertyErrorKind::WrongType {
                        expected: "WM_SIZE_HINTS",
                        got: atoms.format(reply.type_),
                    })
                } else {
                    Ok(WmSizeHints::from_reply(&reply)?)
                }
            }),
        )
    }
}

/// A handle to a response from the X11 server.
pub struct PropertyCookie<'a, T> {
    window: Window,
    atoms: Arc<Atoms>,
    prop: Atom,
    conv: CookieConvFn<'a, T>,
    cookie: Cookie<'a, RustConnection, GetPropertyReply>,
}

impl<'a, T> PropertyCookie<'a, T> {
    pub fn reply(self) -> Result<Option<T>, PropertyError> {
        let wrapper = || {
            let r = self.cookie.reply()?;

            if r.type_ == AtomEnum::NONE.into() {
                Ok(None)
            } else {
                Ok(Some((self.conv)(r, self.atoms.clone())?))
            }
        };
        wrapper().map_err(|kind| PropertyError {
            kind,
            prop: self.atoms.format(self.prop),
            window: self.window,
        })
    }
}

/// An error during a response lookup.
#[derive(Debug)]
pub struct PropertyError {
    kind: PropertyErrorKind,
    prop: String,
    window: Window,
}

impl Display for PropertyError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} (property {:?} of window {})",
            self.kind, self.prop, self.window
        )
    }
}

impl std::error::Error for PropertyError {}

/// An error during a response lookup.
#[derive(Error, Debug)]
pub enum PropertyErrorKind {
    #[error(display = "failed to get a reply: {}", _0)]
    ReplyError(#[source] ReplyError),

    #[error(display = "failed to parse the reply: {}", _0)]
    ParseError(#[source] ParseError),

    #[error(
        display = "value had the wrong format; expected: {}, got: {}",
        expected,
        got
    )]
    WrongFormat { expected: u8, got: u8 },

    #[error(
        display = "value hat the wrong type; expected: {}, got: {}",
        expected,
        got
    )]
    WrongType { expected: &'static str, got: String },

    #[error(
        display = "value's length was wrong; expected: {}, got: {}",
        expected,
        got
    )]
    WrongLength { expected: u32, got: u32 },
}
