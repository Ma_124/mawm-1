//! Implementation for the X window interface.

mod atoms;
mod ewmh;
mod events;
mod prop;
mod state;

/// A struct with fields for Atom names with the numeric
/// identifiers as values.
pub use atoms::Atoms;

/// A cookie for use with [`Atoms::new`].
pub use atoms::AtomsCookie;

pub use events::*;
pub use prop::*;
pub use state::*;
