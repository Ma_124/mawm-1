use std::fmt::Debug;
use std::sync::Arc;
use tracing::error;
use x11rb::{
    connection::Connection,
    protocol::xproto::{
        AtomEnum, ChangeWindowAttributesAux, ConnectionExt as _, EventMask, PropMode, Window,
    },
    rust_connection::RustConnection,
    wrapper::ConnectionExt as _,
};

use crate::{x::Atoms, FatalError};

/// A struct wrapping X information.
pub struct XState {
    pub(super) conn: Arc<RustConnection>,
    pub(super) root_window: Window,
    pub(super) atoms: Arc<Atoms>,
}

impl Debug for XState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("XState")
            .field("conn", &"...")
            .field("root_window", &self.root_window)
            .field("atoms", &self.atoms)
            .finish()
    }
}

impl XState {
    pub fn new(conn: Arc<RustConnection>, screen_num: usize) -> Result<Self, FatalError> {
        let root_window = conn.setup().roots[screen_num].root;

        // Intern atom names
        let atoms = match Atoms::new(conn.as_ref())?.reply() {
            Ok(atoms) => atoms,
            Err(error) => {
                return Err(FatalError::XReplyError {
                    error,
                    function: "failed to intern atoms".into(),
                });
            }
        };

        Ok(XState {
            conn,
            root_window,
            atoms: Arc::new(atoms),
        })
    }

    pub fn atoms(&self) -> Arc<Atoms> {
        self.atoms.clone()
    }

    /// Create a new [`XState`] and register as the WM.
    pub fn register_wm(&self) -> Result<(), FatalError> {
        // Setup event mask
        // Most importantly SUBSTRUCTURE_REDIRECT which registers us as the window manager
        if let Err(error) = self
            .conn
            .change_window_attributes(
                self.root_window,
                &ChangeWindowAttributesAux::new().event_mask(
                    EventMask::SUBSTRUCTURE_REDIRECT
                        | EventMask::SUBSTRUCTURE_NOTIFY
                        | EventMask::ENTER_WINDOW
                        | EventMask::LEAVE_WINDOW
                        | EventMask::PROPERTY_CHANGE,
                ),
            )?
            .check()
        {
            error!(hint = "is another WM running?");
            return Err(FatalError::XReplyError {
                error,
                function: "failed register event mask".into(),
            });
        }

        // TODO Create support window & assign props

        // Set atoms
        self.conn.change_property32(
            PropMode::REPLACE,
            self.root_window,
            self.atoms._NET_SUPPORTED,
            AtomEnum::ATOM,
            &self.atoms.net_atoms(),
        )?;

        Ok(())
    }

    /// List all children of the root window.
    pub fn children(&self) -> Result<Vec<Window>, FatalError> {
        Ok(self
            .conn
            .query_tree(self.root_window)?
            .reply()
            .map_err(|error| FatalError::XReplyError {
                error,
                function: "get children of root".into(),
            })?
            .children)
    }

}
