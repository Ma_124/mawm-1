use std::sync::Arc;

use x11rb::protocol::{xproto::AtomEnum, Event as XEvent};

use crate::Event;

use super::Atoms;

// TODO handle client messages, see dwm: clientmessage (fullscreen, urgent)
// TODO handle configure request, see dwm: configurerequest (pos, size, stacking)
// TODO handle configure notification, see dwm: configurenotify (root window changed size)
// TODO handle focusin event, see dwm: focusin (window requests focus)
// TODO handle enter notification, see dwm: enternotify (focus changed because of mouse)
// TODO handle motion notification, see dwm: motionnotify (cursor moved to other screen)
// TODO handle mouse button, see dwm: buttonpress (focus changed because of mouse, restack)

pub fn translate_event(a: &Arc<Atoms>, ev: &XEvent) -> Option<Event> {
    match ev {
        XEvent::MapRequest(ev) => Some(Event::ManageRequest(ev.window)),
        XEvent::UnmapNotify(ev) => Some(Event::UnmanageRequest(ev.window)),
        XEvent::PropertyNotify(ev) => {
            if ev.atom == a._NET_WM_NAME {
                Some(Event::TitleChange(ev.window))
            } else if ev.atom == AtomEnum::WM_CLASS.into() {
                Some(Event::ClassChange(ev.window))
            } else if ev.atom == a._NET_WM_PID {
                Some(Event::PidChange(ev.window))
            } else if ev.atom == a._NET_WM_WINDOW_TYPE {
                Some(Event::TypeChange(ev.window))
            } else if ev.atom == a._NET_WM_STATE {
                Some(Event::StateChange(ev.window))
            } else if ev.atom == AtomEnum::WM_SIZE_HINTS.into() {
                Some(Event::SizeHintsChange(ev.window))
            } else if ev.atom == AtomEnum::WM_TRANSIENT_FOR.into() {
                Some(Event::TransientForChange(ev.window))
            } else if ev.atom == AtomEnum::WM_HINTS.into() {
                Some(Event::HintsChange(ev.window))
            } else {
                None
            }
        }
        _ => None,
    }
}
