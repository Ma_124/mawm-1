use clap::Parser;
use std::{io, sync::Arc};

use mawm::{x::XState, FatalError};
use tracing_subscriber::EnvFilter;

#[derive(Parser, Debug)]
struct Cli {
    /// Only list windows with titles
    #[clap(short = 't', long)]
    has_title: bool,

    /// Exclude windows with `override_redirect = true`
    #[clap(short, long)]
    redirect: bool,
}

fn main() -> Result<(), FatalError> {
    let cli: Cli = Cli::parse();

    tracing_subscriber::fmt()
        .with_writer(io::stderr)
        .with_env_filter(EnvFilter::from_default_env())
        .init();

    let (conn, screen_num) = x11rb::connect(None)?;
    let x = XState::new(Arc::new(conn), screen_num)?;

    let mut errs = Vec::new();

    for child in x.children()? {
        match x.new_client(child, true, cli.redirect) {
            Ok(Some(child)) => {
                if cli.has_title && child.title.is_empty() {
                    continue;
                }
                println!("{:#?}", child)
            }
            Ok(None) => {},
            Err(err) => errs.push(err),
        }
    }

    if !errs.is_empty() {
        eprintln!("=========================================");
        for err in errs {
            eprintln!("{}", err);
        }
    }

    Ok(())
}
