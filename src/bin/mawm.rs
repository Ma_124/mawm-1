use std::io;

use mawm::{run, FatalError};
use tracing::error;
use tracing_subscriber::EnvFilter;

fn main() {
    tracing_subscriber::fmt()
        .with_writer(io::stderr)
        .with_env_filter(EnvFilter::from_default_env())
        .init();

    match run() {
        Ok(()) => {}
        Err(FatalError::XConnectionError(error)) => {
            error!(msg = %format!("{}", error), ?error, "connection failed");
        }
        Err(error) => {
            error!(msg = %format!("{}", error), ?error, "error");
        }
    }
}
