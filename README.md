# MAWM
An X window manager in Rust.
Why? Because why not.

## Goals
1. Compact
2. Minimal Window Decorations
3. Layouts
4. Controlled by `sxhkd`-like daemon

## Prior Art
- [DWM: A popular, compact WM written in C](https://dwm.suckless.org/)
- [LeftWM: A popular, configurable WM written in Rust](https://github.com/leftwm/leftwm)
- [GabelstaplerWM: An obscure, compact WM written in Rust](https://github.com/ibabushkin/gabelstaplerwm)
- [XCB DWM: An abandoned rewrite of DWM using XCB](https://github.com/julian-goldsmith/dwm-xcb)

## License
Copyright 2022 Ma_124 <ma_124+oss@pm.me>

Licensed under either of
   Apache License, Version 2.0 or
   MIT License
at your option.

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
License, shall be dual licensed as above, without any additional terms
or conditions.
